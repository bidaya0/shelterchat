import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// #docregion MyApp
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // #docregion build
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Startup Name Generator',
      home: RandomWordsState23(),
    );
  }

// #enddocregion build
}

// #enddocregion MyApp
class _RandomWordsState1 extends State<RandomWordsState23> {
  final words = <String>["今天就到这里", "分析费50法郎"];
  final wordssender = <String>["123123", "123312123"];
  String messagestr = "";
  String adduserstr = "";

  void _incrementCounter() {
    setState(() {
      words.add("value");
    });
  }

  void _addString() {
    setState(() {
      words.add(messagestr);
    });
  }

  void _changeString(String k1) {
    messagestr = k1;
  }

  void _changeUserNameString(String k1) {
    adduserstr = k1;
  }

  @override
  Widget build(BuildContext context) {
    // return ListView(
    //   padding: const EdgeInsets.all(16.0),
    //   children: words.map((word) =>  ListTile(title: Text(word))).toList()
    //   ,
    // );
    return Scaffold(
      appBar: AppBar(title: const Text('瞎聊会'), actions: [
        IconButton(
          icon: const Icon(Icons.playlist_add),
          iconSize: 20.0,
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("增加用户"),
                  content: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text("名字"),
                        TextField(
                          autocorrect: false,
                          onChanged: _changeUserNameString,
                        ),
                        Text("颜色"),
                        TextField(
                          autocorrect: false,
                          onChanged: _changeUserNameString,
                        )
                      ]),
                  actions: [
                    TextButton(
                      onPressed: () {},
                      child: Text("创建用户"),
                    )
                  ],
                );
              },
            );
          },
        ),
        IconButton(
          icon: const Icon(Icons.list),
          iconSize: 20.0,
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return Container(
                  child: Text("23123132"),
                );
              },
            );
          },
        )
      ]),
      body: ListView.separated(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (context, i) {
          return ListTile(
            title: Text(words[i]),
            subtitle: Text(wordssender[i]),
          );
        },
        itemCount: words.length,
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
      bottomSheet: Container(
          padding: const EdgeInsets.symmetric(vertical: 2.0),
          child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            // First child is enter comment text input
            Expanded(
              child: TextField(
                autocorrect: false,
                decoration: const InputDecoration(
                  labelText: "Some Text",
                  labelStyle: TextStyle(fontSize: 20.0, color: Colors.white),
                  fillColor: Colors.blue,
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.purpleAccent)),
                ),
                onChanged: _changeString,
              ),
            ),
            // Second child is button
            IconButton(
              icon: const Icon(Icons.send),
              iconSize: 20.0,
              color: Colors.red,
              onPressed: _addString,
            )
          ])),
    );
  }
}

class RandomWordsState23 extends StatefulWidget {
  @override
  State createState() => _RandomWordsState1();
}
